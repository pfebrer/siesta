# ---
# Copyright (C) 1996-2021	The SIESTA group
#  This file is distributed under the terms of the
#  GNU General Public License: see COPYING in the top directory
#  or http://www.gnu.org/copyleft/gpl.txt .
# See Docs/Contributors.txt for a list of contributors.
# ---
.SUFFIXES: .f .F .o .a .f90 .F90

EXE = tshs2tshs
default: $(EXE)

TOPDIR=.
MAIN_OBJDIR=.

#override WITH_MPI=
override WITH_NCDF_PARALLEL=

VPATH=$(TOPDIR)/Util/TS/tshs2tshs:$(TOPDIR)/Src
TOPSRC=$(TOPDIR)/Src

ARCH_MAKE=$(MAIN_OBJDIR)/arch.make

include $(ARCH_MAKE)
include $(MAIN_OBJDIR)/check_for_build_mk.mk

# Set the default non-mpi routines
FC_DEFAULT  := $(FC)
FC_SERIAL   ?= $(FC_DEFAULT)


# Only because the NetCDF libraries could be parallel, in
# which case we need the mpi-compiler :(
# However, we still compile everything else as though it did not exist
#FC:=$(FC_SERIAL)         # Make it non-recursive
# (With the override of MPI suggested above, one should add the appropriate
# libraries to NETCDF_LIBS...)

FPPFLAGS += $(DEFS_PREFIX)-UMPI 
#FPPFLAGS += $(DEFS_PREFIX)-UNCDF_PARALLEL

INCFLAGS:=$(NETCDF_INCFLAGS) $(INCFLAGS)
INCFLAGS+=$(LIBSYS_INCFLAGS)

# Uncomment the following line for debugging support
#FFLAGS=$(FFLAGS_DEBUG)

SYSOBJ=$(SYS).o

# systematic siesta files
DEP_OBJS = precision.o parallel.o \
        alloc.o pxf.o units.o  \
	memory_log.o m_io.o io.o \
	m_sparse.o intrinsic_missing.o geom_helper.o reclat.o \
	io_sparse.o ncdf_io.o m_ts_io.o

# Add class objects
DEP_OBJS += m_uuid.o object_debug.o \
	class_Sparsity.o class_OrbitalDistribution.o \
	class_Data1D.o class_Data2D.o \
	class_SpData1D.o class_SpData2D.o

# Add sys object
DEP_OBJS += $(SYSOBJ)

DEP_OBJS += m_os.o
DEP_OBJS += m_ts_io_version.o

LOCAL_OBJS = tshs2tshs.o

OBJS=$(DEP_OBJS) $(LOCAL_OBJS)

COMP_LIBS := $(filter-out libsiestaLAPACK.a,$(COMP_LIBS))
COMP_LIBS := $(filter-out libsiestaBLAS.a,$(COMP_LIBS))

#######################
#  TS-TBT libs:
#
# Libraries used by transiesta and tbtrans
# They are currently supplied by this distribution
# All rights reserved for the author of these libraries.
# Author: Nick Papior Andersen
#
INCFLAGS += $(FDICT_INCFLAGS) $(NCDF_INCFLAGS)
#######################
#
$(EXE): $(COMP_LIBS) $(TSHS_COMP_LIBS) $(OBJS) $(LIBSYS)
	$(FC) $(FFLAGS) $(LDFLAGS) -o $(EXE) \
		$(OBJS) $(COMP_LIBS) $(TSHS_COMP_LIBS) $(LIBSYS) $(LIBS) 

clean:
	@echo "==> Cleaning object, library, and executable files"
	rm -f $(EXE) *.o *.a *.mod

PROGS:= tshs2tshs
install: $(PROGS)
	cp -p $(PROGS) $(SIESTA_INSTALL_DIRECTORY)/bin

# Dependencies
.PHONY: dep
dep:
	@sfmakedepend --depend=obj --modext=o \
		$(addprefix $(TOPSRC)/,$(DEP_OBJS:.o=.f) $(DEP_OBJS:.o=.f90)) \
		$(addprefix $(TOPSRC)/,$(DEP_OBJS:.o=.F) $(DEP_OBJS:.o=.F90)) \
		$(TOPSRC)/class*.T90 \
		$(LOCAL_OBJS:.o=.f90) $(LOCAL_OBJS:.o=.F90) \
		$(LOCAL_OBJS:.o=.f) $(LOCAL_OBJS:.o=.F) || true
	@sed -i -e 's/\.T90\.o:/.o:/g' Makefile

# DO NOT DELETE THIS LINE - used by make depend
class_Data1D.o: alloc.o
class_Data2D.o: alloc.o
class_Data3D.o: alloc.o
class_SpData1D.o: class_Data1D.o class_Data1D.o class_Data1D.o class_Data1D.o
class_SpData1D.o: class_Data1D.o
class_SpData1D.o: class_OrbitalDistribution.o class_Sparsity.o
class_SpData2D.o: class_Data2D.o class_Data2D.o class_Data2D.o
class_SpData2D.o: class_OrbitalDistribution.o class_Sparsity.o
class_SpData3D.o: class_OrbitalDistribution.o class_Sparsity.o
class_Sparsity.o: alloc.o
class_TriMat.o: alloc.o intrinsic_missing.o
geom_helper.o: intrinsic_missing.o
io.o: m_io.o
io_sparse.o: class_OrbitalDistribution.o class_SpData1D.o class_SpData2D.o
io_sparse.o: class_Sparsity.o precision.o
m_sparse.o: alloc.o class_OrbitalDistribution.o class_SpData2D.o
m_sparse.o: class_Sparsity.o geom_helper.o intrinsic_missing.o parallel.o
m_sparse.o: precision.o
m_ts_io.o: alloc.o class_OrbitalDistribution.o class_SpData1D.o
m_ts_io.o: class_SpData2D.o class_Sparsity.o geom_helper.o io_sparse.o m_os.o
m_ts_io.o: m_sparse.o memory_log.o ncdf_io.o parallel.o precision.o 
memory_log.o: m_io.o parallel.o precision.o
ncdf_io.o: class_OrbitalDistribution.o class_SpData1D.o class_SpData2D.o
ncdf_io.o: class_Sparsity.o io_sparse.o parallel.o precision.o
units.o: precision.o
tshs2tshs.o: class_OrbitalDistribution.o class_SpData1D.o class_SpData2D.o
tshs2tshs.o: class_Sparsity.o geom_helper.o m_sparse.o m_ts_io.o parallel.o
tshs2tshs.o: precision.o units.o
class_ddata1d.o: class_Data1D.o
class_ddata2d.o: class_Data2D.o
class_dspdata1d.o: class_SpData1D.o
class_dspdata2d.o: class_SpData2D.o
class_gdata1d.o: class_Data1D.o
class_gspdata1d.o: class_SpData1D.o
class_idata1d.o: class_Data1D.o
class_idata2d.o: class_Data2D.o
class_ispdata1d.o: class_SpData1D.o
class_ispdata2d.o: class_SpData2D.o
class_ldata1d.o: class_Data1D.o
class_lspdata1d.o: class_SpData1D.o
class_sdata1d.o: class_Data1D.o
class_sspdata1d.o: class_SpData1D.o
class_zdata1d.o: class_Data1D.o
class_zdata2d.o: class_Data2D.o
class_zspdata1d.o: class_SpData1D.o
class_zspdata2d.o: class_SpData2D.o
io_sparse_m.o: io_sparse.o
m_object_debug.o: object_debug.o
mtprng.o: m_uuid.o
ncdf_io_m.o: ncdf_io.o
