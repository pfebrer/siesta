---
project: SIESTA
project_gitlab: https://gitlab.com/siesta-project/siesta
project_download: https://gitlab.com/siesta-project/siesta
project_website: https://siesta-project.org
summary: ![SIESTA](media/SIESTA-logo.png)
         A first-principles materials simulation code 
         using Density Functional Theory.
         {: style="text-align: center"}
author: SIESTA Authors
github: https://gitlab.com/siesta-project/siesta/-/blob/master/Docs/Contributors.txt
predocmark: >
predocmark_alt: <
docmark_alt: *
preprocess: true
preprocessor: gfortran -E -P
fpp_extensions: F90
                F
license: gfdl
extra_filetypes: sh #
                 inc ! 
md_extensions: markdown.extensions.toc
src_dir: @CMAKE_SOURCE_DIR@/Src
exclude_dir: @CMAKE_SOURCE_DIR@/Src/MPI
             @CMAKE_SOURCE_DIR@/Src/fdf
             @CMAKE_SOURCE_DIR@/Src/fdict
             @CMAKE_SOURCE_DIR@/Src/ncdf
             @CMAKE_SOURCE_DIR@/Src/Libs
             @CMAKE_SOURCE_DIR@/Src/Orphans
exclude:   m_gauss_fermi_17.f90
		   m_gauss_fermi_18.f90
		   m_gauss_fermi_19.f90
		   m_gauss_fermi_20.f90
		   m_gauss_fermi_22.f90
		   m_gauss_fermi_24.f90
		   m_gauss_fermi_26.f90
		   m_gauss_fermi_28.f90
		   m_gauss_fermi_30.f90
		   m_gauss_fermi_inf.f90
		   spinorbit.f
media_dir: @CMAKE_SOURCE_DIR@/Docs/developer/ford-media
md_base_dir: @CMAKE_SOURCE_DIR@
page_dir: @CMAKE_SOURCE_DIR@/Docs/developer/ford-pages
output_dir: @FORD_OUTPUT_DIR@
display: public
         protected
         private
source: true
graph: false
search: false
---

--------------------

You can explore the tabs at the top.

For further information, see the [documentation site](https://docs.siesta-project.org).

License
-------

SIESTA's source code and related files and documentation are distributed under the GPL. See the [COPYING](https://gitlab.com/siesta-project/siesta/-/blob/master/COPYING) file for more details.


